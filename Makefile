CC ?= clang
CFLAGS ?= -Wall -pedantic
LDFLAGS ?= -lX11

ywm: ywm.o
	$(CC) $(CFLAGS) -o $@ $+ $(LDFLAGS)

clean:
	rm -f ywm *.o
