#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <X11/cursorfont.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>

#define DIE(...) { fprintf(stderr, __VA_ARGS__); exit(EXIT_FAILURE); }

typedef struct {
    Window window; // assumes valid window ids are always nonzero
                   // uses managed_window.window as truthy or falsy
    int key;
    int pid; // TODO use at some point
    int is_active;
} managed_window;

typedef void (*tiling_policy)(Window window, int index, int nwindows, unsigned width, unsigned height);

typedef union {
    char *const *argv;
    tiling_policy tiling_mode;
    size_t index;
    unsigned key;
} arguments;

typedef struct {
    KeySym keysym;
    unsigned modifiers;
    void (*action)(managed_window *, arguments);
    arguments arguments;
} key_binding;

const unsigned BORDER_WIDTH = 1;
const unsigned BORDER_COLOR_FOCUSED = 0x00ff00;
const unsigned BORDER_COLOR_UNFOCUSED = 0xff0000;
#define MAX_WINDOWS 200

Display *display;
Window root_window;
int running;

managed_window managed_windows[MAX_WINDOWS];

tiling_policy tiling_mode;
void draw_windows(tiling_policy);

int max(int x, int y) {
    return x >= y ? x : y;
}

void move_resize(Window window, int x, int y, unsigned width, unsigned height) {
    XMoveResizeWindow(display, window, x, y, width-2*BORDER_WIDTH, height-2*BORDER_WIDTH);
}

void focus(Window window) {
    XSetInputFocus(display, window, RevertToParent, CurrentTime);
}

// Ensures that the given window is mapped
// Returns 1 if the window was newly mapped, 0 if the window was mapped already
int show_window(managed_window *window) {
    if(!window->is_active) {
        window->is_active = 1;
        XMapWindow(display, window->window);
        focus(window->window);
        return 1;
    }
    else {
        return 0;
    }
}

// Ensures that the given window is unmapped
// Return 1 if the window was newly unmapped, 0 if the window was unmapped already
int hide_window(managed_window *window) {
    if(window->is_active) {
        window->is_active = 0;
        XUnmapWindow(display, window->window);
        return 1;
    }
    else {
        return 0;
    }
}

void quit(managed_window *_, arguments __) {
    running = 0;
}

void spawn(managed_window *_, arguments arguments) {
    if(fork() == 0) {
        execvp(arguments.argv[0], arguments.argv);
        DIE("execvp failed\n");
    }
}

void show_window_by_index(managed_window *_, arguments arguments) {
    managed_window *window = &managed_windows[arguments.index];
    if(!window->window) {
        return;
    }

    if(show_window(window)) {
        draw_windows(tiling_mode);
    }
}

void hide_window_by_index(managed_window *_, arguments arguments) {
    managed_window *window = &managed_windows[arguments.index];
    if(!window->window) {
        return;
    }

    if(hide_window(window)) {
        draw_windows(tiling_mode);
    }
}

void toggle_window_by_index(managed_window *_, arguments arguments) {
    (managed_windows[arguments.index].is_active ? hide_window_by_index : show_window_by_index)(_, arguments);
}

void show_windows_by_key(managed_window *_, arguments arguments) {
    int some_new_window_shown = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window && managed_windows[i].key == arguments.key && show_window(&managed_windows[i])) {
            some_new_window_shown = 1;
        }
    }

    if(some_new_window_shown) {
        draw_windows(tiling_mode);
    }
}

void hide_windows_by_key(managed_window *_, arguments arguments) {
    int some_new_window_hidden = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window && managed_windows[i].key == arguments.key && hide_window(&managed_windows[i])) {
            some_new_window_hidden = 1;
        }
    }

    if(some_new_window_hidden) {
        draw_windows(tiling_mode);
    }
}

// Shows all windows with assigned key.
// Only if all windows with assigned key are already shown, hides hides all windows with assigned key instead.
void toggle_windows_by_key(managed_window *_, arguments arguments) {
    int some_window_not_already_shown = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window && managed_windows[i].key == arguments.key && !managed_windows[i].is_active) {
            some_window_not_already_shown = 1;
            break;
        }
    }

    (some_window_not_already_shown ? show_windows_by_key : hide_windows_by_key)(_, arguments);
}

void claim_window_by_key(managed_window *current_window, arguments arguments) {
    if(current_window) {
        current_window->key = arguments.key;
    }
}

void hide_all_windows(managed_window *_, arguments __) {
    int some_new_window_hidden = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window && hide_window(&managed_windows[i])) {
            some_new_window_hidden = 1;
        }
    }

    if(some_new_window_hidden) {
        draw_windows(tiling_mode);
    }
}

void set_tiling_mode(managed_window *_, arguments arguments) {
    tiling_mode = arguments.tiling_mode;
    draw_windows(tiling_mode);
}

void horizontal_tiling(Window window, int index, int nwindows, unsigned width, unsigned height) {
    move_resize(window, index*width/nwindows, 0, width/nwindows, height);
}

void vertical_tiling(Window window, int index, int nwindows, unsigned width, unsigned height) {
    move_resize(window, 0, index*height/nwindows, width, height/nwindows);
}

const unsigned GAP_WIDTH = 30;
const unsigned GAP_HEIGHT = 30;

void horizontal_tiling_gaps(Window window, int index, int nwindows, unsigned width, unsigned height) {
    unsigned usable_width = max(width - (nwindows + 1) * GAP_WIDTH, 0);
    unsigned usable_height = max(height - 2 * GAP_HEIGHT, 0);

    move_resize(window, index * (usable_width/nwindows) + (index+1) * GAP_WIDTH, GAP_HEIGHT, usable_width/nwindows, usable_height);
}

void vertical_tiling_gaps(Window window, int index, int nwindows, unsigned width, unsigned height) {
    unsigned usable_width = max(width - 2 * GAP_WIDTH, 0);
    unsigned usable_height = max(height - (nwindows + 1) * GAP_HEIGHT, 0);

    move_resize(window, GAP_WIDTH, index * (usable_height/nwindows) + (index+1) * GAP_HEIGHT, usable_width, usable_height/nwindows);
}

char *const SuperReturn_args[] = {"st", NULL};

const unsigned ModMask = Mod1Mask;
key_binding key_bindings[] = {
    {.keysym = XK_q, .modifiers = ModMask | ShiftMask, .action = quit},
    {.keysym = XK_Escape, .modifiers = ModMask, .action = hide_all_windows},

    {.keysym = XK_Return, .modifiers = ModMask, .action = spawn, .arguments = {.argv = SuperReturn_args}},
    {.keysym = XK_v, .modifiers = ModMask, .action = set_tiling_mode, .arguments = {.tiling_mode = vertical_tiling_gaps}},
    {.keysym = XK_h, .modifiers = ModMask, .action = set_tiling_mode, .arguments = {.tiling_mode = horizontal_tiling_gaps}},

    {.keysym = XK_1, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 0}},
    {.keysym = XK_2, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 1}},
    {.keysym = XK_3, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 2}},
    {.keysym = XK_4, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 3}},
    {.keysym = XK_5, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 4}},
    {.keysym = XK_6, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 5}},
    {.keysym = XK_7, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 6}},
    {.keysym = XK_8, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 7}},
    {.keysym = XK_9, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 8}},
    {.keysym = XK_0, .modifiers = ModMask, .action = toggle_window_by_index, .arguments = {.index = 9}},

    {.keysym = XK_a, .modifiers = ModMask | ShiftMask, .action = claim_window_by_key, .arguments = {.key = XK_a}},
    {.keysym = XK_a, .modifiers = ModMask, .action = toggle_windows_by_key, .arguments = {.key = XK_a}},
};

void grab_keys(Window);

managed_window *managed_window_by_id(Window window) {
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window == window) {
            return &managed_windows[i];
        }
    }
    return NULL;
}

// Creates a managed_window entry for a given window id
// Returns the address of the created entry or NULL if there is no space left
managed_window *add_window(Window window) {
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(!managed_windows[i].window) {
            managed_windows[i] = (managed_window){.window = window, .key = 0, .pid = 0, .is_active = 0};
            return &managed_windows[i];
        }
    }
    return NULL;
}

void remove_window(managed_window *managed) {
    *managed = (managed_window){.window = 0, .key = 0, .pid = 0, .is_active = 0};
}

// Performs action for any occurrence of window in managed_windows
// Returns number of encounters
int do_for_window(Window window, void (*action)(managed_window *)) {
    int count = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window == window) {
            action(&managed_windows[i]);
            count++;
        }
    }

    return count;
}

void draw_windows(tiling_policy tiling_mode) {
    int width = XDisplayWidth(display, XDefaultScreen(display));
    int height = XDisplayHeight(display, XDefaultScreen(display));

    int nwindows = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window && managed_windows[i].is_active) {
            nwindows++;
        }
    }
    int index = 0;
    for(int i = 0; i < MAX_WINDOWS; i++) {
        if(managed_windows[i].window && managed_windows[i].is_active) {
            tiling_mode(managed_windows[i].window, index, nwindows, width, height);
            index++;
        }
    }
}

tiling_policy tiling_mode = vertical_tiling_gaps;

void on_create_notify(const XEvent *f) {
    const XCreateWindowEvent *e = &f->xcreatewindow;
    Window window = e->window;
    printf("CreateNotify %lu\n", window);

    // TODO XSelectInput(display, e->xcreatewindow->window, FocusChangeMask | EnterWindow | LeaveWindow);
    // TODO grab buttons/keys
}

void on_destroy_notify(const XEvent *f) {
    const XDestroyWindowEvent *e = &f->xdestroywindow;
    Window window = e->window;
    printf("DestroyNotify %lu\n", window);

    // Only redraw if, window could be found in managed_windows and removed
    if(do_for_window(window, remove_window)) {
        draw_windows(tiling_mode);
    }
}

void on_map_request(const XEvent *f) {
    const XMapRequestEvent *e = &f->xmaprequest;
    Window window = e->window;
    printf("MapRequest %lu\n", window);

    managed_window *managed_window = add_window(window); // TODO NULL handling
    show_window(managed_window);
    XSetWindowBorderWidth(display, window, BORDER_WIDTH);
    XSetWindowBorder(display, window, BORDER_COLOR_UNFOCUSED);
    XSelectInput(display, window, EnterWindowMask | LeaveWindowMask);

    grab_keys(window);

    draw_windows(tiling_mode);
}

void on_configure_request(const XEvent *f) {
    const XConfigureRequestEvent *e = &f->xconfigurerequest;

    XWindowChanges changes;
    changes.x = e->x;
    changes.y = e->y;
    changes.height = e->height;
    changes.width = e->width;
    changes.border_width = e->border_width;
    changes.sibling = e->above;
    changes.stack_mode = e->detail;

    XConfigureWindow(display, e->window, e->value_mask, &changes);
}

void on_enter_notify(const XEvent *f) {
    const XCrossingEvent *e = &f->xcrossing;
    Window window = e->window;
    printf("EnterNotify %lu\n", window);

    focus(window);
}

void on_leave_notify(const XEvent *f) {
    const XCrossingEvent *e = &f->xcrossing;
    Window window = e->window;
    printf("LeaveNotify %lu\n", window);
}

void on_key_press(const XEvent *f) {
    const XKeyPressedEvent *e = &f->xkey;
    printf("KeyPress %u %u\n", e->keycode, e->state);

    for(int i = 0; i < sizeof(key_bindings) / sizeof(*key_bindings); i++) {
        if(XKeysymToKeycode(display, key_bindings[i].keysym) == e->keycode && key_bindings[i].modifiers == e->state) {
            managed_window *managed_window = managed_window_by_id(e->window);
            key_bindings[i].action(managed_window, key_bindings[i].arguments);
            break;
        }
    }
}

void (*event_handlers[])(const XEvent *) = {
    [CreateNotify] = on_create_notify,
    [DestroyNotify] = on_destroy_notify,
    [MapRequest] = on_map_request,
    [ConfigureRequest] = on_configure_request,
    [EnterNotify] = on_enter_notify,
    [LeaveNotify] = on_leave_notify,
    [KeyPress] = on_key_press
};

void grab_key(Window window, unsigned keycode, unsigned modifiers) {
    XGrabKey(display, keycode, modifiers, window, True, GrabModeAsync, GrabModeAsync);
}

void grab_keys(Window window) {
    for(int i = 0; i < sizeof(key_bindings) / sizeof(*key_bindings); i++) {
        grab_key(window, XKeysymToKeycode(display, key_bindings[i].keysym), key_bindings[i].modifiers);
    }
}

void handle_event(const XEvent *e) {
    if(sizeof(event_handlers) / sizeof(*event_handlers) > e->type) {
        if(event_handlers[e->type]) {
            event_handlers[e->type](e);
        }
    }
}

int main() {
    display = XOpenDisplay(NULL); // TODO NULL
    if(display == NULL) {
        DIE("Could not open display");
    }
    root_window = DefaultRootWindow(display);

    // TODO check if other WM is already running
    XSelectInput(display, root_window, KeyPressMask | EnterWindowMask | LeaveWindowMask | SubstructureRedirectMask | SubstructureNotifyMask);
    // TODO XSetErrorHandler

    // Set cursor
    Cursor cursor = XCreateFontCursor(display, XC_left_ptr);
    XDefineCursor(display, root_window, cursor);
    XFreeCursor(display, cursor);

    for(running = 1; running; ) {
        XEvent e;
        XNextEvent(display, &e);
        handle_event(&e);
    }

    XCloseDisplay(display);
    exit(EXIT_SUCCESS);
}
